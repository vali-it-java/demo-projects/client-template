
// Functions for communicating with backend-API here...

function fetchSomething() {
    return fetch(
        `${API_URL}/something`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}

function postSomething(something) {
    return fetch(
        `${API_URL}/something`, 
        {
            method: 'POST', 
            body: JSON.stringify(something)
        }
    );
}

function deleteSomething(id) {
    return fetch(
        `${API_URL}/something/${id}`,
        {
            method: 'DELETE'
        }
    );
}
